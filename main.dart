import 'package:animatedo_app/src/page/first_page.dart';
import 'package:animatedo_app/src/page/navigation_page.dart';
import 'package:flutter/material.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Animate do',
      home: FirstPage()
    );
  }
}