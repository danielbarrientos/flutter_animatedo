import 'package:animate_do/animate_do.dart';
import 'package:animatedo_app/src/page/navigation_page.dart';
import 'package:animatedo_app/src/page/twitter_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class FirstPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animate_do'),
        actions: [
          IconButton(icon: FaIcon(FontAwesomeIcons.twitter), onPressed: (){
             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TwitterPage()));
          }),
          SlideInLeft(
            from: 50,
            child: IconButton(icon: FaIcon(Icons.navigate_next), onPressed: (){
              Navigator.push(context, CupertinoPageRoute(builder: (BuildContext context) => FirstPage()));
            }),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElasticIn(
              delay: Duration(milliseconds: 1100),
              child: Icon(Icons.new_releases, color: Colors.blue, size: 40)
            ),

            FadeInDown(
              delay: Duration(milliseconds: 200),
              child: Text('Título', style:  TextStyle(fontSize: 40, fontWeight: FontWeight.w200))
            ),
            FadeInDown(
              delay: Duration(milliseconds: 1000),
              child: Text('texto pequeño', style:  TextStyle(fontSize: 20, fontWeight: FontWeight.w200)),
            ),
            FadeInLeft(
              delay: Duration(milliseconds: 1100),
              child: Container(
                width: 220,
                height: 2,
                color: Colors.blue,
              ),
            )
          ],
        ),
      ),
      floatingActionButton: ElasticInRight(
              child: FloatingActionButton(
          child: FaIcon(FontAwesomeIcons.play),
          onPressed: (){
             Navigator.push(context, CupertinoPageRoute(builder: (BuildContext context) => NavigationPage()));
          },
        ),
      ),
   );
  }
}