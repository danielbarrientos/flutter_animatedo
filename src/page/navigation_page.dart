import 'dart:ffi';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';


class NavigationPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      create: (_) => _NotifocationModel() ,

      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pink,
          title: Text('Notification page'),
        ),
      
        bottomNavigationBar: BottomNavigation(),
          floatingActionButton: FloatingButton( ),
   ),
    );
  }
}

class BottomNavigation extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final int number = Provider.of<_NotifocationModel>(context).number;

    return BottomNavigationBar(
      currentIndex: 0,
      selectedItemColor: Colors.pink,
      items: [
        BottomNavigationBarItem(label: 'Bones',icon: FaIcon(FontAwesomeIcons.bone)),
        BottomNavigationBarItem(
          label: 'Notifications',
          icon: Stack(
            children:[ 
              FaIcon(FontAwesomeIcons.bell),
              Positioned(
                top: 0, 
                right: -0.5, 
                // child: Icon(Icons.brightness_1,size: 8,color: Colors.pink,)
                child: BounceInDown(
                  from: 10,
                  animate: (number > 0)?true:false,
                  child: Bounce(
                    from: 10,
                    controller: (controller)=> Provider.of<_NotifocationModel>(context).bounceControler = controller,
                    child: Container(
                      width: 13,
                      height: 13,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.pink,
                        shape: BoxShape.circle
                      ),  
                      child: Text('$number', style: TextStyle(color: Colors.white,fontSize: 8),),
                    ),
                  ),
                ),
              )
            ]
          )
        ),
        BottomNavigationBarItem(label: 'Dog',icon: FaIcon(FontAwesomeIcons.dog)),
      ]
    );
  }
}

class FloatingButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Colors.pink,
      child: FaIcon(FontAwesomeIcons.play),
      onPressed: (){
        final model = Provider.of<_NotifocationModel>(context, listen: false);

        int newNumber = model.number;
        newNumber++;
        model.number = newNumber;

        if(newNumber >= 2) {
          final controller =model.bounceControler;
          controller.forward(from: 0.0);
        }

      }
    );
  }
}

class _NotifocationModel extends ChangeNotifier {

  int _number = 0;
  AnimationController _bounceControler;

  get number => this._number;

  set number(int value){
    this._number = value;
    notifyListeners();
  }

  AnimationController get bounceControler => this._bounceControler; 

  set bounceControler(AnimationController controller) {
    this._bounceControler = controller;
    notifyListeners();
  }
}